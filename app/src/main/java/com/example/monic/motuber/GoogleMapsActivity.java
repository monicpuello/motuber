package com.example.monic.motuber;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SearchView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.monic.motuber.models.Carrera;
import com.example.monic.motuber.models.User;
import com.example.monic.motuber.models.Vehiculo;
import com.example.monic.motuber.util.AjaxRequest;
import com.example.monic.motuber.util.Helper;
import com.example.monic.motuber.util.MyLocation;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class GoogleMapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnMarkerDragListener {

    private GoogleMap mMap;

    private RequestQueue requestQueue;
    private AjaxRequest ajaxRequest;
    private SharedPreferences sharedPreferences;

    private String user_nombres;
    private int user_id;
    private String placa;
    private String access_token;
    private String[] busquedas;
    private String dirOrigen;
    private String dirDestino;

    private MarkerOptions marker_driver;
    private Marker marker_destino;
    private Marker selected_marker;
    private Marker marker_conductor;
    private Vehiculo selected_moto;
    private Carrera carrera;
    private Map<Integer, Marker> motos_markers;
    private Map<Integer, Vehiculo> motos;
    private Polyline ruta_origen;
    private Polyline ruta_destino;
    private MyLocation myLocation;

    private TextView tvPlaca;
    private TextView tvOrigen;
    private TextView tvDestino;
    private TextView tvRecorrido;
    private TextView tvPrecio;
    private TextView tvUserNombre;
    private TextView tvUserTelefono;
    private TextView tvNombres;
    private TextView tvVehichulo;
    private TextView tvVehiculoTitulo;
    private RelativeLayout rlMoto;
    private RelativeLayout rlCAccepted;
    private Button btnAceptar;
    private Button btnCancelar;
    private ImageView pacman;
    private SearchView svBuscar;

    private final static int LOCATION_REQUEST = 500;

    private boolean pause_interval = false;
    private boolean zoom_bounds = true;
    private boolean a_bordo = false;
    private boolean con_pasajero = false;
    private boolean carrera_aceptada;
    private boolean avisado = false;
    private LatLngBounds.Builder builder;

    Locale locale;
    NumberFormat currencyFormatter;

    Handler handler;

    @SuppressLint("UseSparseArrays")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        if (access_token.length() == 0) {
            goMain();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_maps);

        tvOrigen = (TextView) findViewById(R.id.tvOrigen);
        tvDestino =  (TextView) findViewById(R.id.tvDestino);
        tvRecorrido = (TextView) findViewById(R.id.tvRecorrido);
        tvPrecio = (TextView) findViewById(R.id.tvPrecio);
        rlMoto = (RelativeLayout) findViewById(R.id.rlMoto);
        btnAceptar = (Button) findViewById(R.id.btnAceptarCarrera);
        btnCancelar = (Button) findViewById(R.id.btnCancelarCarrera);
        pacman = (ImageView) findViewById(R.id.pacman);
        tvUserNombre = (TextView) findViewById(R.id.tvUserNombre);
        tvUserTelefono = (TextView) findViewById(R.id.tvUserTelefono);
        tvNombres = (TextView) findViewById(R.id.tvNombres);
        svBuscar = (SearchView) findViewById(R.id.svBuscar);
        tvVehichulo = (TextView) findViewById(R.id.tvVehiculo);
        tvVehiculoTitulo = (TextView) findViewById(R.id.tvVehiculoTitulo);

        user_id = sharedPreferences.getInt("user_id", 0);
        user_nombres = sharedPreferences.getString("user_nombres", "");
        access_token = sharedPreferences.getString("access_token", "");

        if (user_id == 0 || access_token.length() == 0) {
            Toast.makeText(this, "No hay ids en esta sesión.", Toast.LENGTH_SHORT).show();
            return;
        }

        tvNombres.setText(user_nombres);

        requestQueue = Volley.newRequestQueue(this);

        ajaxRequest = new AjaxRequest(this, requestQueue, access_token);

        myLocation = new MyLocation();

        motos_markers = new HashMap<>();
        motos = new HashMap<>();

        locale = new Locale("es", "CO");
        currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        handler = new Handler();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void loadContactos(View view) {
        Intent intent = new Intent(this, Contactos.class);
        startActivity(intent);
    }

    private void clearMap() {
        //mMap.clear();
        for(Marker marker : motos_markers.values()) {
            marker.setVisible(false);
        }
        if (ruta_origen != null) {
            ruta_origen.remove();
        }
        if (ruta_destino != null) {
            ruta_destino.remove();
        }
        if (marker_destino != null) {
            marker_destino.remove();
        }
        //pause_interval = true;
        //Helper.collapse(rlCarrera);
        rlMoto.setVisibility(View.GONE);
        focusMe();
    }

    private void updateDisponible(int activo) {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/users/" + user_id;

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("activo", activo + "");

        ajaxRequest.put(url, jsonParams, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.i("Mot updateDisponible", "Actualizó bien");
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (carrera == null) {
                    if (marker_destino == null) {
                        MarkerOptions mo = Helper.createMarker(latLng.latitude, latLng.longitude, R.drawable.marker_user_destino);
                        marker_destino = mMap.addMarker(mo);
                    } else {
                        marker_destino.setPosition(latLng);
                    }
                    btnAceptar.setVisibility(View.VISIBLE);
                    tvUserNombre.setVisibility(View.INVISIBLE);
                    tvUserTelefono.setVisibility(View.INVISIBLE);
                    if (myLocation.getLocation() != null) {
                        loadRuteTo(marker_destino);
                    }
                }
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST);
            return;
        }

        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(this);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 30, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    myLocation.setLocation(location);
                    updateMyLocation(myLocation.getLatLng());
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }

        prepareBusqueda();

        updateData();
    }

    private void prepareBusqueda() {
        final View view = this.getCurrentFocus();
        svBuscar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (carrera == null) {
                    // Check if no view has focus:
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    query += " Cartagena Colombia";
                    buscarDireccion(query.replace(" ", "+"));
                } else {
                    Toast.makeText(GoogleMapsActivity.this, "Ya tienes una carrera en curso", Toast.LENGTH_SHORT).show();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void buscarDireccion(String direccion) {
        String url = getGoolePlacesUrl(direccion);
        Toast.makeText(this, "Buscando dirección...", Toast.LENGTH_SHORT).show();
        ajaxRequest.get(url, null, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    JSONArray results = response.getJSONArray("results");
                    JSONObject jo = results.getJSONObject(0);
                    JSONObject geometry = jo.getJSONObject("geometry");
                    JSONObject location = geometry.getJSONObject("location");
                    Double lat = location.getDouble("lat");
                    Double lng = location.getDouble("lng");
                    LatLng latLng = new LatLng(lat, lng);
                    MarkerOptions mo = Helper.createMarker(lat, lng, R.drawable.marker_user_destino);
                    mo.position(latLng);
                    mo.draggable(true);
                    if (marker_destino != null) {
                        marker_destino.remove();
                    }
                    marker_destino = mMap.addMarker(mo);
                    focusTo(latLng);
                    loadRuteTo(marker_destino);
                    tvPrecio.setVisibility(View.INVISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(GoogleMapsActivity.this, "No se encontró la dirección", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void confirmarCarrera(View view) {
        loadRuteTo(marker_destino);
    }

    @SuppressLint("MissingPermission")
    private void updateData() {
        boolean canAnimateZoom = myLocation.getLocation() == null, updateLocation = false;

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location actualLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), false));

        if (myLocation.getLocation() != null && (actualLocation.getLatitude() != myLocation.getLocation().getLatitude() || actualLocation.getLongitude() != myLocation.getLocation().getLongitude())) {
            updateLocation = true;
        }

        if (actualLocation != null) {
            myLocation.setLocation(actualLocation);
        }

        if (canAnimateZoom && myLocation.getLatLng() != null && marker_destino == null) {
            focusMe();
        }

        if (!a_bordo) {
            loadMotosMarkers();
        }

        if (updateLocation) {
            if (marker_destino != null) {
                loadRuteTo(marker_destino);
            }
            updateMyLocation(myLocation.getLatLng());
        }

        if (carrera != null) {
            updateCarrera();
        }

        if (!pause_interval) {
            handler.postDelayed(new Runnable() {
                public void run() {
                    updateData();
                }
            }, 3000);
        }
    }

    private void loadMotosMarkers() {
        loadMotos();

        //pacman.setVisibility(View.INVISIBLE);

        if (motos_markers.isEmpty()) {
            for (Vehiculo moto : motos.values()) {
                motos_markers.put(moto.getId(), mMap.addMarker(moto.getUser().getPosicion()));
            }
        } else if (!motos.isEmpty()) {
            ArrayList<Integer> removes = new ArrayList();
            for (Map.Entry<Integer, Marker> entry : motos_markers.entrySet()) {
                Vehiculo moto = motos.get(entry.getKey());
                Marker marker = entry.getValue();
                if (moto == null) {
                    marker.remove();
                    removes.add(entry.getKey());
                } else {
                    if (!marker.getPosition().equals(moto.getUser().getPosicion().getPosition())) {
                        marker.setPosition(moto.getUser().getPosicion().getPosition());
                    }
                }
            }
            for (int i : removes) {
                motos_markers.remove(i);
            }
            for (Vehiculo moto : motos.values()) {
                if (motos_markers.get(moto.getId()) == null) {
                    motos_markers.put(moto.getId(), mMap.addMarker(moto.getUser().getPosicion()));
                }
            }
        } else {
            if (carrera == null)
                Toast.makeText(this, "No hay motos disponibles", Toast.LENGTH_SHORT).show();
            for (Marker marker: motos_markers.values()) {
                marker.remove();
            }
            motos_markers.clear();
            zoom_bounds = true;
        }

        if (marker_destino == null) {
            zoom_bounds = true;
        }

        if (zoom_bounds && !motos_markers.isEmpty()) {
            //focusAllMarkers();
            zoom_bounds = false;
        } else if (motos_markers.isEmpty() && marker_destino == null) {
            focusMe();
        }
    }

    private void loadMotos() {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/users/activos";

        ajaxRequest.get(url, null, new AjaxRequest.ResponseArray() {
            @Override
            public void onSuccess(JSONArray response) {
                try {
                    motos = new HashMap<>();
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject usuario = response.getJSONObject(i);

                        User user = new User();
                        user.setNombres(usuario.getString("nombres"));
                        user.setCelular(usuario.getString("celular"));
                        user.setEmail(usuario.getString("email"));
                        //user.setCalificacion(usuario.getDouble("promedio"));
                        user.setLat(usuario.getDouble("lat"));
                        user.setLng(usuario.getDouble("lng"));
                        MarkerOptions posicion = Helper.createMarker(user.getLat(), user.getLng(), null, R.drawable.moto);
                        user.setPosicion(posicion);

                        JSONObject co = usuario.getJSONObject("vehiculo");
                        Vehiculo moto = new Vehiculo();
                        moto.setId(co.getInt("id"));
                        moto.setUserId(co.getInt("user_id"));
                        moto.setPlaca(co.getString("placa"));
                        moto.setMarca(co.getString("marca"));
                        moto.setModelo(co.getString("modelo"));
                        moto.setColor(co.getString("color"));
                        moto.setUser(user);

                        motos.put(moto.getId(), moto);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateMyLocation(LatLng latLng) {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/users/" + user_id;

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("lat", Double.toString(latLng.latitude));
        jsonParams.put("lng", Double.toString(latLng.longitude));
        if (carrera != null && !a_bordo) {
            // Si aún no han aceptado mi carrera, actualizar tambien posición origen
            jsonParams.put("carrera_id", carrera.getId() + "");
        }

        ajaxRequest.put(url, jsonParams, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.i("Mot updateMyLocation", "Actualizó bien");
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (myLocation.getLocation() == null) {
            Toast.makeText(this, "No se ha determinado la ubicación del dispositivo, intente de nuevo.", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!marker.equals(marker_destino)) {
            marker_destino.remove();
            marker_destino = null;
        }
        loadRuteTo(marker);
        return true;
    }



    private void loadRuteTo(Marker marker) {
        String url;
        builder = LatLngBounds.builder();

        if (carrera == null) {
            tvVehiculoTitulo.setVisibility(View.INVISIBLE);
            tvVehichulo.setVisibility(View.INVISIBLE);
        }

        if (marker_destino != null && (!carrera_aceptada || a_bordo)) {
            url = getGooleRouteUrl(myLocation.getLatLng(), marker_destino.getPosition());
            loadDirections(url, true);
            builder.include(myLocation.getLatLng());
            builder.include(marker_destino.getPosition());
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 60));
            rlMoto.setVisibility(View.VISIBLE);
        } else if (marker_destino != null && carrera_aceptada) {
            url = getGooleRouteUrl(myLocation.getLatLng(), marker_conductor.getPosition());
            loadDirections(url, false);
            builder.include(myLocation.getLatLng());
            builder.include(marker_conductor.getPosition());
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 40));
        } else {
            url = getGooleRouteUrl(myLocation.getLatLng(), marker.getPosition());
            loadDirections(url, false);
            builder.include(myLocation.getLatLng());
            builder.include(marker.getPosition());
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 40));
            Vehiculo moto = motos.get(Helper.getKeyByValue(motos_markers, marker));
            if (moto != null) {
                selected_marker = marker;
                tvUserNombre.setVisibility(View.GONE);
                tvUserTelefono.setVisibility(View.GONE);
                rlMoto.setVisibility(View.VISIBLE);
                btnAceptar.setVisibility(View.GONE);
                btnCancelar.setVisibility(View.GONE);
            }
        }
    }

    private void loadDirections(String url, final boolean destino) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray routes = response.getJSONArray("routes");
                            if (routes.length() > 0) {
                                JSONObject route = routes.getJSONObject(0);
                                JSONArray legs = route.getJSONArray("legs");
                                JSONObject leg = legs.getJSONObject(0);
                                JSONObject distance = leg.getJSONObject("distance");
                                String distancia = distance.getString("text");
                                JSONObject duration = leg.getJSONObject("duration");
                                String duracion = duration.getString("text");
                                String direccion_origen = leg.getString("start_address");
                                String direccion_destino = leg.getString("end_address");
                                JSONArray steps = leg.getJSONArray("steps");

                                PolylineOptions polylineOptions = new PolylineOptions();

                                ArrayList<LatLng> puntos = new ArrayList<>();

                                for (int i = 0; i < steps.length(); i++) {
                                    JSONObject step = steps.getJSONObject(i);
                                    JSONObject polyline = step.getJSONObject("polyline");
                                    String points = polyline.getString("points");
                                    List points_decoded = Helper.decodePolyline(points);
                                    for (Object latLng : points_decoded) {
                                        puntos.add((LatLng) latLng);
                                        if (marker_destino != null && builder != null) {
                                            builder.include((LatLng) latLng);
                                        }
                                    }
                                }

                                polylineOptions.addAll(puntos);
                                polylineOptions.color(R.color.colorAccent);
                                polylineOptions.geodesic(true);

                                if (destino || selected_moto == null) {
                                    if (ruta_destino != null) {
                                        ruta_destino.remove();
                                    }
                                    ruta_destino = mMap.addPolyline(polylineOptions);

                                    tvOrigen.setText(direccion_origen.replace(", Cartagena, Bolívar, Colombia", ""));
                                    tvDestino.setText(direccion_destino.replace(", Cartagena, Bolívar, Colombia", ""));
                                    if (!(carrera != null && carrera.getEstado().equals("A"))) {
                                        tvRecorrido.setText(distancia + " ~" + duracion);
                                    }
                                    dirOrigen = direccion_origen;
                                    dirDestino = direccion_destino;
                                } else {
                                    if (ruta_origen != null) {
                                        ruta_origen.remove();
                                    }
                                    ruta_origen = mMap.addPolyline(polylineOptions);
                                    if (selected_moto == null) {
                                        //ruta_origen.setVisible(false);
                                        tvRecorrido.setText(distancia + " ~" + duracion);
                                    }
                                }

                                if (carrera != null) {
                                    int distancia_metros = distance.getInt("value");
                                    if (carrera.getEstado().equals("E") && distancia_metros <= 30 && !avisado) {
                                        Toast.makeText(GoogleMapsActivity.this, "El conductor está cerca", Toast.LENGTH_SHORT).show();
                                        avisado = true;
                                    } else if (carrera.getEstado().equals("B") && distancia_metros <= 30 && a_bordo) {
                                        Toast.makeText(GoogleMapsActivity.this, "Has llegado a tu destino", Toast.LENGTH_SHORT).show();
                                        updateEstadoCarrera("R"); // Se llegó al destino
                                        // Mandamos a calificar
                                        carreraRealizada();
                                    }
                                }

                                //marker.setSnippet("Distancia: " + distancia + "\nDuración: " + duracion);
                                //marker.showInfoWindow();
                            } else {
                                Toast.makeText(GoogleMapsActivity.this, "No se encontraron rutas", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        requestQueue.add(request);
    }

    public void solicitarMoto(View view) {
        final EditText edtText = new EditText(this);
        edtText.setInputType(InputType.TYPE_CLASS_NUMBER);
        edtText.setLeft(20);
        edtText.setRight(20);
        tvPrecio.setVisibility(View.INVISIBLE);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Precio");
        builder.setMessage("Cuánto deseas pagar por la carrera");
        builder.setCancelable(false);
        builder.setView(edtText);
        builder.setNeutralButton("SOLICITAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (edtText.getText().toString().length() > 0) {
                    if (ruta_origen != null)
                        ruta_origen.setVisible(false);
                    if (ruta_destino != null)
                        ruta_destino.setVisible(true);
                    btnAceptar.setVisibility(View.GONE);
                    btnCancelar.setVisibility(View.VISIBLE);
                    ((TextView) findViewById(R.id.tvRecorridoTitulo)).setText("Estado");
                    tvRecorrido.setText("Esperando confirmación");
                    tvPrecio.setText(currencyFormatter.format(Integer.parseInt(edtText.getText().toString())));
                    tvPrecio.setVisibility(View.VISIBLE);
                    createCarrera();
                } else {
                    dialog.cancel();
                    Toast.makeText(GoogleMapsActivity.this, "Precio inválido", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void createCarrera() {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/carreras";

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_id", Integer.toString(user_id));
        jsonParams.put("origen_lat", Double.toString(myLocation.getLatLng().latitude));
        jsonParams.put("origen_lng", Double.toString(myLocation.getLatLng().longitude));
        jsonParams.put("origen_dir", tvOrigen.getText().toString());
        jsonParams.put("destino_lat", Double.toString(marker_destino.getPosition().latitude));
        jsonParams.put("destino_lng", Double.toString(marker_destino.getPosition().longitude));
        jsonParams.put("destino_dir", tvDestino.getText().toString());
        jsonParams.put("precio", tvPrecio.getText().toString().replace("$", "").replace(".", "").replace(" ", ""));
        jsonParams.put("estado", "A");

        ajaxRequest.post(url, jsonParams, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    carrera = new Carrera();
                    carrera.setId(response.getInt("id"));
                    carrera.setOrigenLat(response.getDouble("origen_lat"));
                    carrera.setOrigenLng(response.getDouble("origen_lng"));
                    carrera.setOrigenDir(response.getString("origen_dir"));
                    carrera.setDestinoLat(response.getDouble("destino_lat"));
                    carrera.setDestinoLng(response.getDouble("destino_lng"));
                    carrera.setDestinoDir(response.getString("destino_dir"));
                    carrera.setPrecio(response.getInt("precio"));
                    carrera.setUserId(response.getInt("user_id"));
                    carrera.setEstado(response.getString("estado"));
                    Toast.makeText(GoogleMapsActivity.this, "Carrera solicitada", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void carreraAceptada() {
        a_bordo = true;
        ruta_origen.setVisible(false);
        ruta_destino.setVisible(true);
        btnAceptar.setVisibility(View.GONE);
        btnCancelar.setVisibility(View.VISIBLE);
        //rlCAccept.setVisibility(View.VISIBLE);
        for (Marker marker : motos_markers.values()) {
            if (!marker.equals(selected_marker)) {
                marker.setVisible(false);
            }
        }
        builder = LatLngBounds.builder();
        builder.include(selected_marker.getPosition());
        builder.include(myLocation.getLatLng());
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 40));
        ((TextView) findViewById(R.id.tvRecorridoTitulo)).setText(R.string.llegada);
        loadRuteTo(selected_marker);
    }

    public void cancelarCarrera(View view) {
        final Button button = (Button) view;
        if (button.getText().equals(getResources().getString(R.string.cancelar))) {
            button.setText(R.string.seguro);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    button.setText(R.string.cancelar);
                }
            }, 3000);
        } else {
            updateEstadoCarrera("X");
            cleanMap();
            Toast.makeText(this, "Carrera cancelada por el usuario", Toast.LENGTH_SHORT).show();
        }
    }

    private void cleanMap() {
        a_bordo = false;
        carrera_aceptada = false;
        if (selected_marker != null) {
            selected_marker = null;
        }
        if (marker_destino != null) {
            marker_destino.remove();
            marker_destino = null;
        }
        if (ruta_origen != null) {
            ruta_origen.remove();
            ruta_origen = null;
        }
        if (ruta_destino != null) {
            ruta_destino.remove();
            ruta_destino = null;
        }
        if (marker_conductor != null) {
            marker_conductor.remove();
            marker_conductor = null;
        }
        rlMoto.setVisibility(View.GONE);
        btnCancelar.setVisibility(View.GONE);
        btnAceptar.setVisibility(View.VISIBLE);
        if (!motos_markers.isEmpty()) {
            focusAllMarkers();
        } else {
            focusMe();
        }
        selected_moto = null;
        carrera = null;
    }

    public void updateEstadoCarrera(final String estado) {
        if (carrera == null) return;
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/carreras/" + carrera.getId();

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("estado", estado);

        ajaxRequest.put(url, jsonParams, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.i("Mot carrera", "Actualizada: " + estado);
            }
        });
    }

    public void updateCarrera() {
        String url = getResources().getString(R.string.API_URL) + "/api/carreras/" + carrera.getId();
        ajaxRequest.get(url, null, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String estado = response.getString("estado");
                    if (estado.equals("E") || estado.equals("B")) {
                        Vehiculo moto = new Vehiculo();
                        JSONObject vehiculo = response.getJSONObject("vehiculo");
                        moto.setPlaca(vehiculo.getString("placa"));
                        moto.setMarca(vehiculo.getString("marca"));
                        moto.setModelo(vehiculo.getString("modelo"));
                        moto.setMarca(vehiculo.getString("marca"));
                        moto.setColor(vehiculo.getString("color"));
                        carrera.setVehiculo(moto);
                        User conductor = new User();
                        JSONObject user = response.getJSONObject("conductor");
                        conductor.setNombres(user.getString("nombres"));
                        conductor.setIdentificacion(user.getString("identificacion"));
                        conductor.setCelular(user.getString("celular"));
                        LatLng latLng = new LatLng(user.getDouble("lat"), user.getDouble("lng"));
                        conductor.setPosicion(Helper.createMarker(latLng.latitude, latLng.longitude, R.drawable.moto));
                        if (marker_conductor == null) {
                            marker_conductor = mMap.addMarker(conductor.getPosicion());
                        } else {
                            marker_conductor.setPosition(latLng);
                        }
                        conductor.setLng(latLng.latitude);
                        conductor.setLng(latLng.longitude);
                        carrera.setConductor(conductor);
                        for (Marker marker: motos_markers.values()) {
                            marker.remove();
                        }
                        loadRuteTo(marker_conductor);
                        if (estado.equals("E") && !carrera.getEstado().equals(estado)) {
                            carrera_aceptada = true;
                            Toast.makeText(GoogleMapsActivity.this, "Un conductor ha aceptado tu carrera", Toast.LENGTH_SHORT).show();
                            tvUserNombre.setText(conductor.getNombres());
                            tvUserNombre.setVisibility(View.VISIBLE);
                            tvUserTelefono.setText(conductor.getCelular());
                            tvUserTelefono.setVisibility(View.VISIBLE);
                            tvVehiculoTitulo.setVisibility(View.VISIBLE);
                            tvVehichulo.setText(moto.getPlaca() + " " + moto.getMarca() + " " + moto.getModelo() + " " + moto.getColor());
                            tvVehichulo.setVisibility(View.VISIBLE);
                        } else if(!estado.equals(carrera.getEstado()) && !a_bordo) {
                            a_bordo = true;
                            enviarNoti("Me he enbarcado en una moto");
                        }
                    } else if (estado.equals("C")) {
                        cleanMap();
                        carrera = null;
                        a_bordo = false;
                        Toast.makeText(GoogleMapsActivity.this, "La carrera fue cancelada por el conductor", Toast.LENGTH_SHORT).show();
                    }
                    carrera.setEstado(estado);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void focusAllMarkers() {
        if (mMap != null) {
            builder = LatLngBounds.builder();
            for (Marker marker : motos_markers.values()) {
                marker.setVisible(true);
                builder.include(marker.getPosition());
            }
            builder.include(myLocation.getLatLng());
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 40));
        }
    }

    private void focusMe() {
        if (myLocation.getLatLng() != null && mMap != null)
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation.getLatLng(), 15));
    }

    private void focusTo(LatLng latLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }

    public void callUser(View view) {
        Uri number = Uri.parse("tel:" + ((TextView) view).getText());
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);
    }

    private String getGooleRouteUrl(LatLng origen, LatLng destino) {
        String str_origen = "origin=" + origen.latitude + "," + origen.longitude;
        String str_destino = "destination=" + destino.latitude + "," + destino.longitude;
        String sensor = "sensor=false";
        String lang = "languaje=es";
        String key = "key=" + getResources().getString(R.string.google_maps_key);
        String params = str_origen + "&" + str_destino + "&" + sensor + "&" + lang + "&" + key;
        String url = "https://maps.googleapis.com/maps/api/directions/json?" + params;
        return url;
    }

    private String getGoolePlacesUrl(String direccion) {
        String address = "address=" + direccion;
        String sensor = "sensor=false";
        String region = "region=co";
        String key = "key=" + getResources().getString(R.string.google_maps_key);
        String params = address + "&" + sensor + "&" + region + "&" + key;
        String url = "https://maps.googleapis.com/maps/api/geocode/json?" + params;
        return url;
    }

    public void goMain() {
        Intent intent = new Intent(this, Main.class);
        startActivity(intent);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(carrera != null) {
            Helper.alert(this, "¡Espera!", "Tienes una carrera activa, ¿Quieres cancelarla?", "Cancelar Carrera", "Cerrar", new Helper.ResponseConfirm() {
                @Override
                public void onAccept(DialogInterface dialog, int id) {
                    updateEstadoCarrera("X");
                    GoogleMapsActivity.super.onBackPressed();
                }

                @Override
                public void onCancel(DialogInterface dialog, int id) {
                    // Todo bien :)
                }
            });
        } else {
            Helper.alert(this, "Salir", "¿Seguro que deseas salir de la aplicación?", "Salir", "Cerrar", new Helper.ResponseConfirm() {
                @Override
                public void onAccept(DialogInterface dialog, int id) {
                    GoogleMapsActivity.this.finishAffinity();
                }

                @Override
                public void onCancel(DialogInterface dialog, int id) {
                    // No pasa nada :)
                }
            });
        }
    }

    @Override
    protected void onPause() {
        pause_interval = true;
        super.onPause();
    }

    @Override
    protected void onResume() {
        pause_interval = false;
        updateData();
        super.onResume();
    }

    @Override
    protected void onStop() {
        pause_interval = true;
        if (carrera != null) {
            //updateEstadoCarrera("X");
        }
        super.onStop();
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        if (ruta_origen != null) {
            ruta_origen.remove();
            ruta_origen = null;
        }
    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        loadRuteTo(marker);
    }

    public void enviarNoti(String mensaje) {
        String url = getResources().getString(R.string.API_URL) + "/api/contactos/enviar";

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("carrera_id", carrera.getId()+"");
        jsonParams.put("mensaje", mensaje);

        Toast.makeText(GoogleMapsActivity.this, "Notificando a tus contactos el estado de la carrera", Toast.LENGTH_SHORT).show();

        ajaxRequest.post(url, jsonParams, new AjaxRequest.ResponseArray() {
            @Override
            public void onSuccess(JSONArray response) {
                Toast.makeText(GoogleMapsActivity.this, "Notificados", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void irCarreras(View view) {
        if (carrera != null && a_bordo) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt("carrera_id", carrera.getId());
            editor.commit();
        }
        Intent intent = new Intent(this, Carreras.class);
        startActivity(intent);
    }

    public void irContactos(View view) {
        Intent intent = new Intent(this, Contactos.class);
        startActivity(intent);
    }

    public void cerrarSesion(View view) {
        Helper.alert(this, "Cerrar Sesión", "Estás seguro que quieres cerrar la sesión?", "Si", "No", new Helper.ResponseConfirm() {
            @Override
            public void onCancel(DialogInterface dialog, int id) {
                // Seguimos
            }

            @Override
            public void onAccept(DialogInterface dialog, int id) {
                if (carrera != null) {
                    updateEstadoCarrera("X");
                }
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("user_id");
                editor.remove("user_nombres");
                editor.remove("access_token");
                editor.commit();
                Intent intent = new Intent(GoogleMapsActivity.this, Main.class);
                startActivity(intent);
            }
        });
    }

    public void carreraRealizada() {
        enviarNoti("He llegado a mi destino");
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("carrera_id", carrera.getId());
        editor.commit();
        cleanMap();
        mMap.clear();
        Intent intent = new Intent(this, Calificar.class);
        startActivity(intent);
    }
}
