package com.example.monic.motuber;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.monic.motuber.util.AjaxRequest;
import com.example.monic.motuber.util.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Locale;

public class Carreras extends AppCompatActivity {

    private RequestQueue requestQueue;
    private SharedPreferences sharedPreferences;
    private String access_token;
    private AjaxRequest ajaxRequest;
    private int user_id;

    private LinearLayout llCarreras;
    private ProgressBar loading;

    Locale locale;
    NumberFormat currencyFormatter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        user_id = sharedPreferences.getInt("user_id", 0);
        if (access_token.length() == 0 || user_id == 0) {
            goMain();
        }

        getSupportActionBar().setTitle("Carreras realizadas");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carreras);

        llCarreras = (LinearLayout) findViewById(R.id.llCarreras);
        loading = (ProgressBar) findViewById(R.id.loading);

        requestQueue = Volley.newRequestQueue(this);
        ajaxRequest = new AjaxRequest(this, requestQueue, access_token);

        locale = new Locale("es", "CO");
        currencyFormatter = NumberFormat.getCurrencyInstance(locale);

        loadCarreras();
    }

    private void loadCarreras() {
        String url = getResources().getString(R.string.API_URL) + "/api/users/" + user_id + "/carreras";

        ajaxRequest.get(url, null, new AjaxRequest.ResponseArray() {
            @Override
            public void onSuccess(JSONArray response) {
                try {
                    loading.setVisibility(View.GONE);
                    getSupportActionBar().setTitle("Carreras realizadas (" + response.length() + ")");
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject carrera = response.getJSONObject(i);
                        final int carrera_id = carrera.getInt("id");
                        LayoutInflater inflater = (LayoutInflater) Carreras.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View ll = inflater.inflate(R.layout.carrera, null);
                        ((TextView) ll.findViewById(R.id.correo)).setText(carrera.getString("origen_dir"));
                        ((TextView) ll.findViewById(R.id.dir_destino)).setText(carrera.getString("destino_dir"));
                        ((TextView) ll.findViewById(R.id.precio)).setText(currencyFormatter.format(carrera.getInt("precio")));
                        ((TextView) ll.findViewById(R.id.calificacion)).setText(getCalificacion(carrera.getString("calificacion")));
                        ((TextView) ll.findViewById(R.id.fecha)).setText(carrera.getString("updated_at"));
                        ((TextView) ll.findViewById(R.id.estado)).setText(getEstado(carrera.getString("estado")));
                        JSONObject vehiculo = carrera.getJSONObject("vehiculo");
                        ((TextView) ll.findViewById(R.id.placa)).setText(vehiculo.getString("placa"));
                        JSONObject conductor = carrera.getJSONObject("conductor");
                        ((TextView) ll.findViewById(R.id.conductor)).setText(conductor.getString("nombres"));
                        if (carrera.getString("calificacion").equals("null")) {
                            ll.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Helper.alert(Carreras.this, "Calificar carrera", "¿Desea calificar esta carrera?", "Si", "No", new Helper.ResponseConfirm() {
                                        @Override
                                        public void onCancel(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }

                                        @Override
                                        public void onAccept(DialogInterface dialog, int id) {
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.putInt("carrera_id", carrera_id);
                                            editor.commit();
                                            Intent intent = new Intent(Carreras.this, Calificar.class);
                                            startActivity(intent);
                                        }
                                    });
                                }
                            });
                        }
                        llCarreras.addView(ll);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private String getCalificacion(String calificacion) {
        if (calificacion.equals("null")) {
            return "S/C";
        }
        return calificacion;
    }

    private String getEstado(String estado) {
        switch (estado) {
            case "R":
                return "Terminada";
            case "C":
                return "Cancelada por el conductor";
            case "X":
                return "Cancelada por ti";
            case "E":
                return "En espera";
            case "B":
                return "En camino";
                default:
                    return "Desconocido";
        }
    }


    public void goMain() {
        Intent intent = new Intent(this, Main.class);
        startActivity(intent);
    }
}
