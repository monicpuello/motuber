package com.example.monic.motuber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.monic.motuber.util.AjaxRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Calificar extends AppCompatActivity {

    private RequestQueue requestQueue;
    private EditText etMensaje;
    private RatingBar rbCalificacion;
    private SharedPreferences sharedPreferences;
    private String access_token;
    private AjaxRequest ajaxRequest;
    private int carrera_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        carrera_id = sharedPreferences.getInt("carrera_id", 36);
        if (access_token.length() == 0 || carrera_id == 0) {
            goMain();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calificar);

        requestQueue = Volley.newRequestQueue(this);

        ajaxRequest = new AjaxRequest(this, requestQueue, access_token);

        etMensaje = (EditText) findViewById(R.id.etMensaje);
        rbCalificacion = (RatingBar) findViewById(R.id.rbCalificacion);

        rbCalificacion.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                Toast.makeText(Calificar.this, "" + rating, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void goMain() {
        Intent intent = new Intent(this, Main.class);
        startActivity(intent);
    }

    public void calificar(View view) {
        String comentario = etMensaje.getText().toString();
        float calificacion = rbCalificacion.getRating();

        if (calificacion == 0) {
            Toast.makeText(this, "Selecciona al menos una estrella", Toast.LENGTH_SHORT).show();
            return;
        }

        ((ProgressBar)findViewById(R.id.pbLoad)).setVisibility(View.VISIBLE);

        String url = getResources().getString(R.string.API_URL) + "/api/carreras/" + carrera_id;

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("calificacion", calificacion+"");
        jsonParams.put("comentario", comentario);

        ajaxRequest.put(url, jsonParams, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.remove("carrera_id");
                editor.commit();
                Toast.makeText(Calificar.this, "Gracias por tu calificación", Toast.LENGTH_SHORT).show();
                ((ProgressBar)findViewById(R.id.pbLoad)).setVisibility(View.INVISIBLE);
                finish();
            }
        });
    }
}
