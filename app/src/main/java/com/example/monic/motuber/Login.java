package com.example.monic.motuber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Button;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.monic.motuber.util.AjaxRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    private ProgressBar progressBar;
    private RequestQueue requestQueue;
    private EditText etEmail;
    private EditText etPass;
    private String access_token;
    private SharedPreferences sharedPreferences;
    private AjaxRequest ajaxRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        if (access_token.length() > 0) {
            goNext();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        getSupportActionBar().hide(); //ocultar ActionBar


        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPass = (EditText) findViewById(R.id.etPass);
        requestQueue = Volley.newRequestQueue(this);

        etEmail.setText(getIntent().getStringExtra("email"));

        ajaxRequest = new AjaxRequest(this, requestQueue, access_token);
    }

    public void regresar(View view) {
        finish();
    }

    public void login(final View view) {
        String email = etEmail.getText().toString();
        String pass = etPass.getText().toString();

        if (email.length() == 0 || pass.length() == 0) {
            Toast.makeText(this, "Complete los datos", Toast.LENGTH_SHORT).show();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        view.setClickable(false);

        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/oauth/token";

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("username", email);
        jsonParams.put("password", pass);
        jsonParams.put("grant_type", "password");
        jsonParams.put("client_id", "2");
        jsonParams.put("client_secret", "Hebes8GZVpR3fJ401eXa9hDWXmys5TvCqgyBOOOs");

        ajaxRequest.post(url, jsonParams, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    String token_type = response.getString("token_type");
                    if (token_type.equals("Bearer")) {
                        access_token = response.getString("access_token");
                        String refresh_token = response.getString("refresh_token");
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("access_token", access_token);
                        editor.putString("refresh_token", refresh_token);
                        editor.putString("token_type", token_type);
                        editor.commit();
                        ajaxRequest.setAccessToken(access_token);
                        Toast.makeText(Login.this, "Ingreso exitoso", Toast.LENGTH_SHORT).show();
                        getUser();
                    } else {
                        Toast.makeText(Login.this, "El tipo de token es incorrecto: " + token_type, Toast.LENGTH_SHORT).show();
                    }
                    progressBar.setVisibility(View.INVISIBLE);
                    view.setClickable(true);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(Login.this, "Ocurrió un error al parsear el JSON", Toast.LENGTH_SHORT).show();
                } finally {
                    progressBar.setVisibility(View.INVISIBLE);
                    view.setClickable(true);
                }
            }
        });
    }

    private void goNext() {
        Intent intent = new Intent(this, GoogleMapsActivity.class);
        startActivity(intent);
    }

    private void getUser() {
        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/user";

        ajaxRequest.get(url, null, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                try {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("user_id", response.getInt("id"));
                    editor.putString("user_nombres", response.getString("nombres"));
                    editor.commit();
                    goNext();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
