package com.example.monic.motuber;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.monic.motuber.util.AjaxRequest;
import com.example.monic.motuber.util.Helper;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class userRegister extends AppCompatActivity {

    private RequestQueue requestQueue;
    private EditText etEmail;
    private EditText etNombres;
    private EditText etCelular;
    private SharedPreferences sharedPreferences;
    private String access_token;
    private AjaxRequest ajaxRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Verificar si ya ha iniciado sesión
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        if (access_token.length() > 0) {
            goNext();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_register_activity);

        requestQueue = Volley.newRequestQueue(this);

        ajaxRequest = new AjaxRequest(this, requestQueue, access_token);

        etEmail = findViewById(R.id.etCorreo);
        etNombres = findViewById(R.id.etNombres);
        etCelular = findViewById(R.id.etTelefono);
    }

    private void goNext() {
        Intent intent = new Intent(this, GoogleMapsActivity.class);
        startActivity(intent);
    }

    public void registrarse(View view) {
        String nombres = etNombres.getText().toString();
        final String email = etEmail.getText().toString();
        String celular = etCelular.getText().toString();
        String tipo = "U"; // Usuario

        if (nombres.length() == 0 || email.length() == 0 || celular.length() == 0) {
            Toast.makeText(this, "Complete los datos", Toast.LENGTH_SHORT).show();
            return;
        }

        Resources resources = getResources();
        String url = resources.getString(R.string.API_URL) + "/api/register";

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("nombres", nombres);
        jsonParams.put("email", email);
        jsonParams.put("celular", celular);
        jsonParams.put("tipo", tipo);

        ajaxRequest.post(url, jsonParams, new AjaxRequest.ResponseObject() {
            @Override
            public void onSuccess(JSONObject response) {
                Helper.alert(userRegister.this,
                        "¡Registro Exitoso!",
                        "Se le ha enviado un código para ingresar y usar el servicio de UberMot.",
                        "Aceptar", new Helper.ResponseAlert() {
                        @Override
                        public void onAccept(DialogInterface dialog, int id) {
                            Intent intent = new Intent(userRegister.this, Login.class);
                            intent.putExtra("email", email);
                            startActivity(intent);
                        }
                });
            }
        });
    }
}
