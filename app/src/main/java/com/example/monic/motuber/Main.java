package com.example.monic.motuber;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Main extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private String access_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        if (access_token.length() > 0) {
            goNext();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        getSupportActionBar().hide(); //ocultar ActionBar
    }

    //Metodo para el boton Ingresar
    public void ingresar_btn (View view){
        Intent ingresar = new Intent (this, Login.class);
        startActivity(ingresar);
    }

    //Metodo para el boton Registarse
    public void registrarse_btn (View view){
        Intent registrarse = new Intent (this, userRegister.class);
        startActivity(registrarse);
    }

    private void goNext() {
        Intent intent = new Intent(this, GoogleMapsActivity.class);
        startActivity(intent);
    }

}
