package com.example.monic.motuber.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.api.CommonStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AjaxRequest {

    private RequestQueue requestQueue;
    private Context context;
    private String access_token;

    public AjaxRequest(Context context, RequestQueue requestQueue, String access_token) {
        this.context = context;
        this.access_token = access_token;
        this.requestQueue = requestQueue;
    }

    public void setAccessToken(String access_token) {
        this.access_token = access_token;
    }

    public void get(String url, Map params, ResponseObject responseCallback) {
        send(Request.Method.GET, url, params, responseCallback);
    }

    public void get(String url, Map params, ResponseArray responseCallback) {
        send(Request.Method.GET, url, params, responseCallback);
    }

    public void post(String url, Map params, ResponseObject responseCallback) {
        send(Request.Method.POST, url, params, responseCallback);
    }

    public void post(String url, Map params, ResponseArray responseCallback) {
        send(Request.Method.POST, url, params, responseCallback);
    }

    public void put(String url, Map params, ResponseObject responseCallback) {
        params.put("_method", "PUT");
        send(Request.Method.POST, url, params, responseCallback);
    }

    public void delete(String url, ResponseObject responseCallback) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("_method", "DELETE");
        send(Request.Method.POST, url, params, responseCallback);
    }

    private void send(int method, String url, Map params, final ResponseObject responseCallback) {
        JSONObject send_params = params == null ? null : new JSONObject(params);
        Log.i("Mot " + (method == 1 ? "POST" : "GET"), url + ": " + (params != null ? send_params.toString() : "{}"));
        JsonObjectRequest request = new JsonObjectRequest(method, url, send_params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.has("success") && response.getBoolean("success")) {
                                responseCallback.onSuccess(response.getJSONObject("data"));
                                Log.w("Mot ajax", response.getString("message"));
                            } else if (response.has("message")) {
                                Log.w("Mot Error: ", response.getString("message"));
                                Toast.makeText(context, response.getString("message"), Toast.LENGTH_LONG).show();
                            } else {
                                responseCallback.onSuccess(response);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Error al parsear JSON", Toast.LENGTH_SHORT).show();
                            Log.i("Mot data", response.toString());
                            Log.e("Mot error", e.getMessage());
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Error en el servidor", Toast.LENGTH_SHORT).show();
                            Log.i("Mot data", response.toString());
                            Log.e("Mot error", e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Toast.makeText(context, networkResponse.statusCode + ": Ocurrió un error, intente de nuevo.", Toast.LENGTH_SHORT).show();
                }
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        requestQueue.add(request);
    }

    private void send(int method, String url, Map params, final ResponseArray responseCallback) {
        JSONObject send_params = params == null ? null : new JSONObject(params);
        Log.i("Mot " + (method == 1 ? "POST" : "GET"), url + ": " + (params != null ? send_params.toString() : "{}"));
        JsonObjectRequest request = new JsonObjectRequest(method, url, send_params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.has("success") && response.getBoolean("success")) {
                                responseCallback.onSuccess(response.getJSONArray("data"));
                                Log.w("Mot ajax", response.getString("message"));
                            } else if (response.has("message")) {
                                Log.w("Mot Error: ", response.getString("message"));
                                Toast.makeText(context, "Mira:" + response.getString("message"), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(context, "Respuesta desconocida", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Error al parsear JSON", Toast.LENGTH_SHORT).show();
                            Log.i("Mot data", response.toString());
                            Log.e("Mot error", e.getMessage());
                        } catch (Exception e) {
                            Toast.makeText(context, "Error en el servidor: ", Toast.LENGTH_SHORT).show();
                            Log.i("Mot data", response.toString());
                            Log.e("Mot error", e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Toast.makeText(context, networkResponse.statusCode, Toast.LENGTH_SHORT).show();
                    if (networkResponse.statusCode == 401) {
                        Toast.makeText(context, "Usuario o contraseña incorrectos.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, networkResponse.statusCode + ": Ocurrió un error, intente de nuevo.", Toast.LENGTH_SHORT).show();
                    }
                }
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer " + access_token);
                return headers;
            }
        };

        requestQueue.add(request);
    }

    public interface ResponseObject {
        void onSuccess(JSONObject response);
    }
    public interface ResponseArray {
        void onSuccess(JSONArray response);
    }
}