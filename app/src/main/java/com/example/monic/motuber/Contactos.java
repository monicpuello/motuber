package com.example.monic.motuber;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.monic.motuber.util.AjaxRequest;
import com.example.monic.motuber.util.Helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Contactos extends AppCompatActivity {

    private RequestQueue requestQueue;
    private SharedPreferences sharedPreferences;
    private String access_token;
    private AjaxRequest ajaxRequest;
    private int user_id;
    private int carrera_id;

    private LinearLayout llContactos;
    private ProgressBar loading;
    private FloatingActionButton btnEnviar;

    CollapsingToolbarLayout collapsing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        access_token = sharedPreferences.getString("access_token", "");
        user_id = sharedPreferences.getInt("user_id", 0);
        carrera_id = sharedPreferences.getInt("carrera_id", 0);
        if (access_token.length() == 0 || user_id == 0) {
            goMain();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactos);
        getSupportActionBar().hide();
        //Titulo de la ActionBar
        collapsing = (CollapsingToolbarLayout)findViewById(R.id.main_collapsing);
        collapsing.setTitle("Contactos");

        llContactos = (LinearLayout) findViewById(R.id.llContactos);
        loading = (ProgressBar) findViewById(R.id.loading);
        btnEnviar = (FloatingActionButton) findViewById(R.id.btnEnviar);

        requestQueue = Volley.newRequestQueue(this);
        ajaxRequest = new AjaxRequest(this, requestQueue, access_token);

        //AlertDialog para llamar al layout dialog_contact
        final FloatingActionButton msgDialog = (FloatingActionButton)findViewById(R.id.createContact);
        msgDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            //Metodo para llamar a la vista de crear contacto
            public void onClick(View view) {
                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(Contactos.this);
                final View mView = getLayoutInflater().inflate(R.layout.dialog_contact, null);
                Button btnagregar = (Button) mView.findViewById(R.id.btnAgregar);
                btnagregar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    //Mensaje al dar click en Añadir contacto
                    public void onClick(final View nueva) {
                        final String nombres = ((TextView)mView.findViewById(R.id.et_nombre)).getText().toString();
                        String celular = ((TextView)mView.findViewById(R.id.et_celular)).getText().toString();
                        final String correo = ((TextView)mView.findViewById(R.id.et_correo)).getText().toString();
                        final boolean activo = ((Switch)mView.findViewById(R.id.s_activar)).isChecked();

                        if (nombres.length() == 0 || correo.length() == 0 || celular.length() == 0) {
                            Toast.makeText(Contactos.this, "Complete los datos", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Snackbar.make(nueva, "Cargando", Snackbar.LENGTH_INDEFINITE).show();

                        String url = getResources().getString(R.string.API_URL) + "/api/contactos";
                        Map<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("user_id", user_id+"");
                        jsonParams.put("nombres", nombres);
                        jsonParams.put("celular", celular);
                        jsonParams.put("correo", correo);
                        jsonParams.put("activo", activo ? "1" : "0");

                        ajaxRequest.post(url, jsonParams, new AjaxRequest.ResponseObject() {
                            @Override
                            public void onSuccess(JSONObject response) {
                                try {
                                    final int contacto_id = response.getInt("id");
                                    Snackbar.make(nueva, "Registrado", Snackbar.LENGTH_INDEFINITE).show();
                                    LayoutInflater inflater = (LayoutInflater) Contactos.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    View ll = inflater.inflate(R.layout.contacto, null);
                                    ((TextView) ll.findViewById(R.id.nombres)).setText(nombres);
                                    ((TextView) ll.findViewById(R.id.correo)).setText(correo);
                                    Switch sw = (Switch) ll.findViewById(R.id.activo);
                                    sw.setChecked(activo);

                                    sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                        @Override
                                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                            String url = getResources().getString(R.string.API_URL) + "/api/contactos/" + contacto_id;
                                            Map<String, String> jsonParams = new HashMap<String, String>();
                                            jsonParams.put("activo", isChecked ? "1" : "0");

                                            ajaxRequest.put(url, jsonParams, new AjaxRequest.ResponseObject() {
                                                @Override
                                                public void onSuccess(JSONObject response) {
                                                    Toast.makeText(Contactos.this, "Actualizado", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    });
                                    llContactos.addView(ll);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                mBuilder.setView(mView);
                AlertDialog dialog = mBuilder.create();
                dialog.show();
            }

        });

        loadContactos();
    }

    private void loadContactos() {
        String url = getResources().getString(R.string.API_URL) + "/api/users/" + user_id + "/contactos";

        ajaxRequest.get(url, null, new AjaxRequest.ResponseArray() {
            @Override
            public void onSuccess(JSONArray response) {
                try {
                    loading.setVisibility(View.GONE);
                    collapsing.setTitle("Contactos (" + response.length() + ")");
                    if (response.length()>0 && carrera_id > 0) {
                        btnEnviar.setVisibility(View.VISIBLE);
                    }
                    for (int i = 0; i < response.length(); i++) {
                        final JSONObject contacto = response.getJSONObject(i);
                        final int contacto_id = contacto.getInt("id");
                        LayoutInflater inflater = (LayoutInflater) Contactos.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View ll = inflater.inflate(R.layout.contacto, null);
                        final String nombres = contacto.getString("nombres");
                        ((TextView) ll.findViewById(R.id.nombres)).setText(nombres);
                        final String correo = contacto.getString("correo");
                        ((TextView) ll.findViewById(R.id.correo)).setText(correo);
                        final boolean activo = contacto.getInt("activo") == 1;
                        final String celular = contacto.getString("celular");
                        Switch sw = (Switch) ll.findViewById(R.id.activo);
                        sw.setChecked(activo);

                        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                String url = getResources().getString(R.string.API_URL) + "/api/contactos/" + contacto_id;
                                Map<String, String> jsonParams = new HashMap<String, String>();
                                jsonParams.put("activo", isChecked ? "1" : "0");

                                ajaxRequest.put(url, jsonParams, new AjaxRequest.ResponseObject() {
                                    @Override
                                    public void onSuccess(JSONObject response) {
                                        Toast.makeText(Contactos.this, "Actualizado", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        });

                        ll.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                final AlertDialog.Builder mBuilder = new AlertDialog.Builder(Contactos.this);
                                final View mView = getLayoutInflater().inflate(R.layout.dialog_contact_edit, null);
                                ((TextView)mView.findViewById(R.id.et_nombre)).setText(nombres);
                                ((TextView)mView.findViewById(R.id.et_correo)).setText(correo);
                                ((TextView)mView.findViewById(R.id.et_celular)).setText(celular);
                                ((Switch)mView.findViewById(R.id.s_activar)).setChecked(activo);
                                Button btnagregar = (Button) mView.findViewById(R.id.btnGuardar);
                                Button btncancelar = (Button) mView.findViewById(R.id.btnCancelar);
                                Button btneliminar = (Button) mView.findViewById(R.id.btnEliminar);
                                btnagregar.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    //Mensaje al dar click en guardar
                                    public void onClick(final View nueva) {
                                        final String nombres = ((TextView)mView.findViewById(R.id.et_nombre)).getText().toString();
                                        String celular = ((TextView)mView.findViewById(R.id.et_celular)).getText().toString();
                                        final String correo = ((TextView)mView.findViewById(R.id.et_correo)).getText().toString();
                                        final boolean activo = ((Switch)mView.findViewById(R.id.s_activar)).isChecked();

                                        if (nombres.length() == 0 || correo.length() == 0 || celular.length() == 0) {
                                            Toast.makeText(Contactos.this, "Complete los datos", Toast.LENGTH_SHORT).show();
                                            return;
                                        }

                                        Snackbar.make(nueva, "Cargando", Snackbar.LENGTH_INDEFINITE).show();

                                        String url = getResources().getString(R.string.API_URL) + "/api/contactos/" + contacto_id;
                                        Map<String, String> jsonParams = new HashMap<String, String>();
                                        jsonParams.put("user_id", user_id+"");
                                        jsonParams.put("nombres", nombres);
                                        jsonParams.put("celular", celular);
                                        jsonParams.put("correo", correo);
                                        jsonParams.put("activo", activo ? "1" : "0");

                                        ajaxRequest.put(url, jsonParams, new AjaxRequest.ResponseObject() {
                                            @Override
                                            public void onSuccess(JSONObject response) {
                                                Snackbar.make(nueva, "Actualizado", Snackbar.LENGTH_INDEFINITE).show();
                                                ((TextView) ll.findViewById(R.id.nombres)).setText(nombres);
                                                ((TextView) ll.findViewById(R.id.correo)).setText(correo);
                                                ((Switch) ll.findViewById(R.id.activo)).setChecked(activo);
                                            }
                                        });
                                    }
                                });
                                btncancelar.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // Cerrar dialog
                                    }
                                });
                                btneliminar.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Helper.alert(Contactos.this, "Eliminar contacto", "¿Está seguro que desea eliminar este contacto?", "Eliminar", "Cancelar", new Helper.ResponseConfirm() {
                                            @Override
                                            public void onCancel(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }

                                            @Override
                                            public void onAccept(DialogInterface dialog, int id) {
                                                String url = getResources().getString(R.string.API_URL) + "/api/contactos/" + contacto_id;
                                                ajaxRequest.delete(url, new AjaxRequest.ResponseObject() {
                                                    @Override
                                                    public void onSuccess(JSONObject response) {
                                                        llContactos.removeView(ll);
                                                        ll.setVisibility(View.GONE);
                                                        Toast.makeText(Contactos.this, "Contacto eliminado", Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                                mBuilder.setView(mView);
                                AlertDialog dialog = mBuilder.create();
                                dialog.show();
                            }
                        });
                        llContactos.addView(ll);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void goMain() {
        Intent intent = new Intent(this, Main.class);
        startActivity(intent);
    }

    public void enviarNoti(View view) {
        final EditText edtMensaje = new EditText(this);
        edtMensaje.setHint("Mensaje");

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Enviar notificación personalizada");
        builder.setMessage("Se enviará el mensaje a todos tus contactos activos");
        builder.setCancelable(false);
        builder.setView(edtMensaje);
        builder.setNeutralButton("ENVIAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                enviarNoti(edtMensaje.getText().toString());
            }
        });
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void enviarNoti(String mensaje) {
        String url = getResources().getString(R.string.API_URL) + "/api/contactos/enviar";

        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("carrera_id", carrera_id+"");
        jsonParams.put("mensaje", mensaje);

        Toast.makeText(Contactos.this, "Notificando a tus contactos el estado de la carrera", Toast.LENGTH_SHORT).show();

        ajaxRequest.post(url, jsonParams, new AjaxRequest.ResponseArray() {
            @Override
            public void onSuccess(JSONArray response) {
                Toast.makeText(Contactos.this, "Notificados", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
